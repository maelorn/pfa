package com.eirbmmk.app.settings;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.MainActivityController;

/**
 * This class correspond to the controller part of the MVC design pattern for Settings
 * functionality.
 */
public class SettingsController extends BaseController {

    /* -------------------- Attributes -------------------- */


    /**
    * Model part of the MVC design pattern
    */
    private SettingsModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private SettingsFragment mFragment;


      /* -------------------- Constructors -------------------- */


    /**
     * Constructor of SettingsController class.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the Settings functionality
     * @param fragment the view of the Settings functionality
     */
    public SettingsController(MainActivityController container, SettingsModel model, SettingsFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;
    }


    /* -------------------- Methods -------------------- */
}
