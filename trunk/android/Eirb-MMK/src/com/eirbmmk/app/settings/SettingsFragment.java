package com.eirbmmk.app.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eirbmmk.app.MainActivity;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;

public class SettingsFragment extends PreferenceFragment {
	
    /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Settings pattern
     */
    private SettingsController mController;

    /**
     *  The rootView
     */
    private View mRootView;

    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public SettingsFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }



    /**
     * Constructor of the SettingsFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including Settings information.
     */
    public SettingsFragment(MainActivityController container, SettingsModel model)
    {

        mController = new SettingsController(container, model, this);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null)
        {
            // the OS destroyed the old fragment and instantiates
            // a new one using the default constructor. This new fragment has an illegal state
            // (no controller and mainActivityController reference).
            // So recreate all the MVC pattern for the fragment.
            // Warning : If the application was completely killed by the OS, the current Fragment
            // will be the home fragment and not this fragment.

            MainActivity mainAc = (MainActivity) getActivity();
            mainAc.recreateCurrentMVCFragment();
            return null;
        }

        mRootView = inflater.inflate(R.layout.fragment_settings, container, false);
        addPreferencesFromResource(R.xml.preferences);
        return mRootView;
    }
}
