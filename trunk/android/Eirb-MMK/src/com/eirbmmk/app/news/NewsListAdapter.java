package com.eirbmmk.app.news;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eirbmmk.app.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * List Adapter class to display news in an custom ListView
 */
public class NewsListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<NewsItem> mNewsItems;
    private final static int MAX_CHARACTERS_CONTENT = 70;

    /**
     * Constructor of NewsListAdapter class
     *
     * @param context application context
     * @param newsItems list of news to display
     */
    public NewsListAdapter(Context context, ArrayList<NewsItem> newsItems){
        mContext = context;
        mNewsItems = newsItems;
    }

    @Override
    public int getCount() {
        return mNewsItems.size();
    }

    @Override
    public Object getItem(int i) {
        return mNewsItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.news_list_item, null);
        }

        ImageView imgIcon = (ImageView)view.findViewById(R.id.news_picture);
        TextView txtTitle = (TextView) view.findViewById(R.id.news_title);
        TextView txtContent = (TextView) view.findViewById(R.id.news_content_preview);
        TextView txtDate = (TextView) view.findViewById(R.id.news_date);

        String content = mNewsItems.get(i).getContent();

        if (content.length() > MAX_CHARACTERS_CONTENT)
            content = content.substring(0, MAX_CHARACTERS_CONTENT - 1) + " ...";

        txtContent.setText(content);
        Date reference = new Date();
        long diff = reference.getTime() -  mNewsItems.get(i).getNewsDate().getTime();
        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diff);
        if(diff < 60)
            txtDate.setText("Il y a " + diffInSec + " secondes");
        else if(diffInSec < 3600){
            if((diffInSec/60) > 1)
                txtDate.setText("Il y a " + diffInSec/60 + " minutes");
            else
                txtDate.setText("Il y a " + diffInSec/60 + " minute");
        }
        else if(diffInSec < 86400){
            if((diffInSec/3600) > 1)
                txtDate.setText("Il y a " + diffInSec/3600 + " heures");
            else
                txtDate.setText("Il y a " + diffInSec/3600 + " heure");
        } else{
            if((diffInSec/86400) > 1)
                txtDate.setText("Il y a " + diffInSec/86400 + " jours");
            else
                txtDate.setText("Il y a " + diffInSec/86400 + " jour");
        }

           if(mNewsItems.get(i).getClass() == Tweet.class){
               Tweet tweet = (Tweet) mNewsItems.get(i);
               txtTitle.setText("@" + tweet.getAccount());
               int iconResourceId = mContext.getResources().getIdentifier("ic_twitter", "drawable", mContext.getPackageName());

               imgIcon.setImageResource(iconResourceId);
           } else if(mNewsItems.get(i).getClass() == SchoolNews.class){
               SchoolNews newsItem = (SchoolNews) mNewsItems.get(i);
               txtTitle.setText(newsItem.getTitle());
               int iconResourceId = mContext.getResources().getIdentifier("ic_school_news_website", "drawable", mContext.getPackageName());
               imgIcon.setImageResource(iconResourceId);
           }

        return view;
    }
}
