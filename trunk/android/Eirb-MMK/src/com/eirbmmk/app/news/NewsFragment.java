package com.eirbmmk.app.news;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;

import java.util.ArrayList;

public class NewsFragment extends BaseFragment {

	 /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC News pattern
     */
    private NewsController mController;



    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public NewsFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }



    /**
     * Constructor of the NewsFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including news information.
     */
    public NewsFragment(MainActivityController container, NewsModel model) {
        mController = new NewsController(container, model, this);

        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        mRootView = inflater.inflate(R.layout.fragment_news, container, false);

        if(!mController.isNetworkConnected()){
            mRootView = inflater.inflate(R.layout.no_internet_connection, container, false);
        } else {
            showLoadingSpinner();
            mController.getNews();
        }
        return mRootView;
    }

    /**
     * Puts News in a ListView through an ArrayAdapter
     *
     * @param news ArrayList of NewsItem containing the News to display
     */
    public void displayNews(final ArrayList<NewsItem> news){
        NewsListAdapter adapter = new NewsListAdapter(mRootView.getContext(), news);
        ListView listView = (ListView) mRootView.findViewById(R.id.newsListView);
        listView.setAdapter(adapter);
        listView.invalidate();
        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mController.displayDetailedNewsFragment(news.get(position));
            }
        });
    }
}