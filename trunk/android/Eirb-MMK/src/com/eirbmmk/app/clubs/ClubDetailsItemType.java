package com.eirbmmk.app.clubs;

/**
 * Enumeration that indicates the club item detail type
 */
public enum ClubDetailsItemType {
    IMAGE,
    PARAGRAPH;
}
