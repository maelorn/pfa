package com.eirbmmk.app.schedule;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eirbmmk.app.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

/**
 * Created by maelorn on 03/03/14.
 *
 */
public class DayViewRenderer extends View {
    private static final int AMPHI = 0xfffff3d4;
    private static final int AMPHI_TEXT = 0xffb38509;
    private static final int TD =  0xffe4ecff;
    private static final int TD_TEXT =  0xff004b62;
    private static final int PARTIEL = 0xffffdede;
    private static final int PARTIEL_TEXT = 0xff931616;
    private static final int OPTION = 0xfff9ffdf;
    private static final int OPTION_TEXT = 0xff6b830a;
    private static final int PROJET = 0xffeeeeee;
    private static final int PROJET_TEXT = 0xff333333;
    private static final int ASSO = 0xff000033;
    public static final int CELL_HEIGHT = 125;
    private  static final int HOUR_CELL_WIDTH = 50;
    private static  final  int HOURX_OFFSET = -40;
    private static  final  int HOURY_OFFSET = 30;
    private ScheduleController scheduleController;
    private View mTemplateView;
    private TextView mEventTime;
    private TextView mClock;
    private TextView mEventTitle;
    private TextView mEventType;
    private TextView mEventLocation;
    private TextView mEventTeacher;
    private TextView map;
    private TextView man;
    private View mEventColourPanel;
    private Time mCurrentDay;
    private ArrayList<ArrayList<Course>> sortedCourse;
    private Hashtable <Course, Integer> positionInRow= new Hashtable<Course, Integer>();
    private Hashtable <Course, Integer> courseWidth = new Hashtable<Course, Integer>();

    /**
     *  Constructor of the class DayViewRenderer
     *  get the different element of the layout
     * @param context
     * @param currentDay
     * @param controller
     */
    public DayViewRenderer(Context context,Time currentDay, ScheduleController controller) {
        super(context);        android.util.Log.i("TEST", "DayViewRenderer instancié");

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mTemplateView = inflater.inflate(R.layout.course, null, false);
        /* get fontawesome from Asset */
        Typeface font = Typeface.createFromAsset( context.getAssets(), "fontawesome-webfont.ttf" );

        /* select and load the right textview */
        mEventTitle = (TextView) mTemplateView.findViewById(R.id.eventTitle);
        mEventTime = (TextView) mTemplateView.findViewById(R.id.eventTime);
        mEventType = (TextView)  mTemplateView.findViewById(R.id.eventType);
        mEventLocation = (TextView) mTemplateView.findViewById(R.id.eventLocation);
        mEventTeacher = (TextView) mTemplateView.findViewById(R.id.eventTeacher);
        map = (TextView) mTemplateView.findViewById(R.id.map);
        man = (TextView) mTemplateView.findViewById(R.id.man);
        mClock = (TextView) mTemplateView.findViewById(R.id.clock);
        scheduleController = controller;
        /* set the font to use */
        mCurrentDay = currentDay;
        map.setTypeface(font);
        man.setTypeface(font);
        mClock.setTypeface(font);
        mEventColourPanel = mTemplateView.findViewById(R.id.eventColourPanel);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Compute the height required to render the view
        // Assume Width will always be MATCH_PARENT.
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = CELL_HEIGHT*14;
        setMeasuredDimension(width, height);
    }

    @Override
    public void onDraw(Canvas canvas){
        android.util.Log.i("TEST", "OnDraw");
        drawBackground(canvas);
        sortedCourse = sortCourseByRow(scheduleController.getNewcourseList());
        drawListEvent(sortedCourse, canvas);
    }

    /**
     *  Draw the background of the day
     * @param canvas
     */
    public void drawBackground(Canvas canvas){
        android.util.Log.i("TEST", "OnBackgroundDraw");
        Paint p = new Paint();
        int y1, y2;
        y1 = 0;
        y2 = CELL_HEIGHT;
        for(int hour = 7; hour <= 20; hour++){
            p.setColor(Color.BLACK);
            p.setTextSize(25);
            p.setAntiAlias(true);
            canvas.drawText("" + hour, HOUR_CELL_WIDTH + HOURX_OFFSET, y1 + HOURY_OFFSET, p);
            p.setAntiAlias(false);
            p.setColor(Color.LTGRAY);
            p.setStyle(Paint.Style.FILL);
            p.setAlpha(50);
            canvas.drawRect(new Rect(0, y1, HOUR_CELL_WIDTH, y2), p);
            p.setColor(Color.WHITE);
            p.setStyle(Paint.Style.FILL);
            p.setAlpha(25);
            canvas.drawRect(new Rect(HOUR_CELL_WIDTH, y1, getWidth(), y2), p);
            y1 = y2;
            y2 += CELL_HEIGHT;
            p.setColor(Color.BLACK);
            p.setAlpha(75);
            canvas.drawLine(0, y1, getWidth(), y1, p);
            //p.setStyle(Paint.Style.STROKE);
            //canvas.drawLine(0, y1 + (y2 - y1)/2, getWidth(), y1 + (y2 - y1)/2, p);


        }
    /* Only need to draw this once */
    p.setColor(Color.BLACK);
    p.setAlpha(50);
    canvas.drawLine(HOUR_CELL_WIDTH, 0, HOUR_CELL_WIDTH, getHeight(), p);
    Time td = new Time();
    td.setToNow();
    if (td.monthDay == mCurrentDay.monthDay){
        p.setColor(Color.BLACK);
        p.setAlpha(255);
        int y = CELL_HEIGHT *(td.hour - 7) + CELL_HEIGHT*(td.minute/60);
        canvas.drawLine(HOUR_CELL_WIDTH, y,getWidth(), y, p);
    }
}

    /**
     *
     * @param type the type of the course
     * @return the color matching the type
     */
    public int getColorFromType(String type){
        if (type.equals("TD")){
            return TD;
        } else if (type.equals("Cours")){
            return AMPHI;
        } else if (type.equals("Enseignement intégré")){
            return OPTION;
        } else if (type.equals("Examen") || type.equals("Rattrapage")){
            return  PARTIEL;
        } else if (type.equals("Projet")){
            return PROJET;
        } else {
            return 0xffeeeeee;
        }

    }

    /**
     *
     * @param type the type of the course
     * @return the textColor matching the type
     */
    public int getTextColorFromType(String type){
        if (type.equals("TD")){
            return TD_TEXT;
        } else if (type.equals("Cours")){
            return AMPHI_TEXT;
        } else if (type.equals("Enseignement intégré")){
            return OPTION_TEXT;
        } else if (type.equals("Examen") || type.equals("Rattrapage")){
            return  PARTIEL_TEXT;
        } else if (type.equals("Projet")){
            return PROJET_TEXT;
        } else {
            return 0xff333333;
        }
    }

    /**
     *  Draw the listed of event on the same row
     * @param courses
     * @param canvas
     */
    public void drawEvent(ArrayList<Course> courses, final Canvas canvas) {
        /* Put the square to the right place */
        // translate(left, top);
        /* top = heure -7 * CELL_HEIGHT */
        /* To change when we want to display event occuring the same hour*/

        int width;
        int height;
        if (courses.isEmpty()){
            return;
        }

        for(Course c: courses) {
            width = (getWidth() - HOUR_CELL_WIDTH)/courseWidth.get(c);
            canvas.save();
            height = scheduleController.getBottomCoordinate(c) - scheduleController.getTopCoordinate(c);
            canvas.translate(HOUR_CELL_WIDTH + width * positionInRow.get(c), scheduleController.getTopCoordinate(c));
            mTemplateView.measure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));

            LinearLayout ll = (LinearLayout) mTemplateView.findViewById(R.id.eventColourPanel);
            ll.setClickable(true);
            ll.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "hi", Toast.LENGTH_SHORT).show();
                }
            });
            ll.layout(0, 0, 0, 0);
            ll.setRight(width);
            ll.setBottom(height);

            mEventTitle.setText(c.getTitle());
            mEventTitle.setTextColor(getTextColorFromType(c.getType()));
            mEventTime.setText(c.getStartTime().format("%H:%M") + " à " + c.getEndTime().format("%H:%M"));
            mEventLocation.setText(c.getLocation());
            mEventType.setText(c.getType() + " - " + c.getGroups());
            mEventTeacher.setText(c.getTeacher());
            if ( mEventColourPanel.getBackground() != null) {
                ((GradientDrawable) mEventColourPanel.getBackground()).setColor(getColorFromType(c.getType()));
            }
            mTemplateView.draw(canvas);
            canvas.restore();
        }
    }

    /**
     *  Draw a sorted list of event
     * @param courseList
     * @param canvas
     */
    public void drawListEvent(ArrayList<ArrayList<Course>> courseList, Canvas canvas){
        for(ArrayList<Course> c: courseList){
            drawEvent(c, canvas);
        }
    }

    /**
     *
     * @param t a Time attribute
     * @return the minute past since midnight
     */
    private int timeToMinute(Time t){
        return t.hour*60 + t.minute;
    }

    /**
     *  set the size of by wich dividing the row and set the position of each courses
     * @param courses list of courses on the same row
     */
    private void stackTweaker(ArrayList<Course> courses){
        Collections.sort(courses, new Comparator<Course>() {
            @Override
            public int compare(Course c1, Course c2) {
                int from_cmp = Time.compare(c1.getStartTime(), c2.getStartTime());
                if(from_cmp != 0)
                    return from_cmp;

                int duration_1 = timeToMinute(c1.getStartTime()) - timeToMinute(c1.getEndTime());
                int duration_2 = timeToMinute(c2.getStartTime()) - timeToMinute(c2.getEndTime());
                return duration_1 < duration_2 ? -1 : 1;
            }
        });
        Hashtable<Integer, Integer> lengths = new Hashtable<Integer, Integer>();
        int h = 0;
        for(Course c: courses){
            boolean broken = false;
            for(int j=0; j< h+1; j++) {
                if(!lengths.containsKey(j) || timeToMinute(c.getStartTime()) >= lengths.get(j)) {
                    positionInRow.put(c, j);
                    lengths.put(j, timeToMinute(c.getEndTime()));
                    broken = true;
                    break;
                }
            }
            if(!broken) {
                h++;
                positionInRow.put(c, h);
                lengths.put(h, timeToMinute(c.getEndTime()));
            }
        }
        for (Course c: courses){
//            Log.i("edt", String.format("width %d, course %s", h, c));
            courseWidth.put(c, h+1);
        }
    }

    /**
     *  Sort the courseList by row
     * @param courses
     * @return a sorted ArrayList
     */
    public ArrayList<ArrayList<Course>> sortCourseByRow(ArrayList <Course> courses){
        ArrayList<ArrayList<Course>> sortedCourses = new ArrayList<ArrayList<Course>>();
        ArrayList <Course> sameRowCourse = new ArrayList<Course>();
        int maxEndHour = -1;
        for(Course c : courses){
            if (timeToMinute(c.getStartTime()) >= maxEndHour){
                sortedCourses.add(sameRowCourse);
                stackTweaker(sameRowCourse);
                sameRowCourse = new ArrayList<Course>();
            }
            int currentEnd = timeToMinute(c.getEndTime());
            if (currentEnd > maxEndHour){
                maxEndHour = currentEnd;
            }
            sameRowCourse.add(c);
        }
        stackTweaker(sameRowCourse);
        sortedCourses.add(sameRowCourse);
        return sortedCourses;
    }
}