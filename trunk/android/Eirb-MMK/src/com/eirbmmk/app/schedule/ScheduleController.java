package com.eirbmmk.app.schedule;

import android.app.Activity;
import android.os.AsyncTask;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.Screen;

import java.util.ArrayList;

/**
 * This class correspond to the controller part of the MVC design pattern for Schedule
 * functionality.
 */
public class ScheduleController extends BaseController {

    /* -------------------- Attributes -------------------- */

    /**
    * Model part of the MVC design pattern
    */
    private ScheduleModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private ScheduleFragment mFragment;


      /* -------------------- Constructors -------------------- */


    /**
     * Constructor of ScheduleController class.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the Schedule functionality
     * @param fragment the view of the Schedule functionality
     */
    public ScheduleController(MainActivityController container, ScheduleModel model, ScheduleFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;
    }

    /**
     * get the course list of the currentDay
     * @return
     */
    public ArrayList<Course> getNewcourseList(){
        return mModel.getListEvent(mFragment.getCurrentDay());
    }

    /**
     *  get the top coordinate of a course
     * @param course
     * @return
     */
    public int getTopCoordinate(Course course){
        return  (course.getStartTime().hour - 7)*125 + course.getStartTime().minute*125/60;
    }

    /**
     * get the bootom coordinate of a course
     * @param course
     * @return
     */
    public int getBottomCoordinate(Course course){
        return  (course.getEndTime().hour - 7)*125 + course.getEndTime().minute*125/60;
    }

    /**
     * get the current activity of the View
     * @return
     */
    public Activity getCurrentActivity(){
        return mFragment.getActivity();
    }

    /**
     * get the data to display afterwards
     */
    public void getData(){
        AsyncTask<Void, Void, Boolean> scheduleRequest = new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mFragment.showLoadingSpinner();
            }

            @Override
            protected Boolean doInBackground(Void... Params) {
                if (!isCancelled()) {
                    mModel.fetchData(mMainActivityController.getUserLogin());
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean ok) {
                mFragment.displayData();
                mFragment.hideLoadingSpinner();

                // remove the task to running tasks list
                mAsyncRunningTasks.remove(this);
            }
        };

        // add the task to running tasks list
        mAsyncRunningTasks.add(scheduleRequest);

        // start the async task
        scheduleRequest.execute();
    }
}
