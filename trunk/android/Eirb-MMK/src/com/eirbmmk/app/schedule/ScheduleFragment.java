package com.eirbmmk.app.schedule;

import android.app.FragmentManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;
public class ScheduleFragment extends BaseFragment {

    /* -------------------- Attributes -------------------- */

    private ViewPager mViewPager;

    private Typeface font;

    private static int mCurrentDayIndex = 0;

    private DaySlidePagerAdapter viewPagerAdapter;

    /**
     * The controller part of the MVC Connection pattern
     */
    private ScheduleController mController;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public ScheduleFragment(){
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }



    /**
     * Constructor of the ScheduleFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including Schedule information.
     */
    public ScheduleFragment(MainActivityController container, ScheduleModel model) {
        mController = new ScheduleController(container, model, this);
        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        mRootView = inflater.inflate(R.layout.fragment_schedule, container, false);
        font = Typeface.createFromAsset(getActivity().getAssets(), "fontawesome-webfont.ttf");
        // Get data to display from Model
        mController.getData();

        return mRootView;
    }

    /**
     *  Display the data, called when all json processing are done
     */
    public void displayData(){
        mViewPager = (ViewPager) mRootView.findViewById(R.id.schedule_viewpager);
        viewPagerAdapter = new DaySlidePagerAdapter(getFragmentManager());
        mViewPager.setAdapter(viewPagerAdapter);

        Button previous = (Button) mRootView.findViewById(R.id.schedule_previous_week);
        Button next = (Button) mRootView.findViewById(R.id.schedule_next_week);
        TextView week = (TextView) mRootView.findViewById(R.id.schedule_week_number);
        previous.setTypeface(font);
        next.setTypeface(font);
        Time today = new Time();
        today.setToNow();

        today.normalize(false);
        week.setText("Semaine " + (today.getWeekNumber() + mCurrentDayIndex / 7));
        mViewPager.setCurrentItem(today.weekDay);
        previous.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if (mCurrentDayIndex > 0) {
                    mCurrentDayIndex -= 7;
                    Time today = new Time();
                    today.setToNow();

                    today.normalize(false);
                    TextView week = (TextView) mRootView.findViewById(R.id.schedule_week_number);
                    week.setText("Semaine " + (today.getWeekNumber() + mCurrentDayIndex / 7));
                    viewPagerAdapter.notifyDataSetChanged();
                }
            }
        });

        next.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                mCurrentDayIndex += 7;
                Time today = new Time();
                today.setToNow();

                today.normalize(false);
                TextView week = (TextView) mRootView.findViewById(R.id.schedule_week_number);
                week.setText("Semaine " + (today.getWeekNumber() + mCurrentDayIndex / 7));
                viewPagerAdapter.notifyDataSetChanged();
            }
        });
    }


    /**
     * get the currentDay
     * @return
     */
    public Time getCurrentDay(){
        return updateCurrentDay(mViewPager.getCurrentItem());
    }

    /**
     * update the currentDay from the position
     * @param position the position in the wiewPager
     * @return
     */
    private Time updateCurrentDay(int position){
        Time currentDay = new Time();
        currentDay.setToNow();
        currentDay.monthDay += position - currentDay.weekDay + mCurrentDayIndex;
        currentDay.normalize(false);
        return currentDay;
    }


    private class DaySlidePagerAdapter extends FragmentStatePagerAdapter {

        public DaySlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Time CurrentDay = updateCurrentDay(position);
            return new DaySlidePagerFragment(CurrentDay, mController);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 7;
        }

        /**
         * get the matching human day for the given Time
         * @param day
         * @return
         */
        public CharSequence getHumanDay(Time day){
            switch (day.weekDay){
                case 0:
                    return "Dimanche";
                case 1:
                    return "Lundi";
                case 2:
                    return "Mardi";
                case 3:
                    return "Mercredi";
                case 4:
                    return "Jeudi";
                case 5:
                    return "Vendredi";
                case 6:
                    return "Samedi";
                default:
                    return "";
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Time currentDay = updateCurrentDay(position);
            return getHumanDay(currentDay) + ", " + currentDay.format("%d/%m");
        }
    }
}
