package com.eirbmmk.app.schedule;


import android.text.format.Time;

/**
 * Created by maelorn on 03/03/14.
 */

public class Course {
    private String title;
    private String type;
    private String teacher;
    private String location;
    private Time startTime;
    private Time endTime;
    private String groups;

    public Course(){

    }
    /**
     * Constructor of the Course class.
     *
     * @param title the title of the course
     * @param type the type of the course
     * @param groups the groups having the course
     * @param teacher the teacher giving the course
     * @param location the location of the course
     * @param startTime the time the course starts
     * @param endTime the time the course ends
     */
    public Course(String title, String type, String groups, String teacher, String location, Time startTime, Time endTime) {
        this.title = title;
        this.type = type;
        this.teacher = teacher;
        this.location = location;
        this.startTime = startTime;
        this.endTime = endTime;
        this.groups = groups;
    }

    /**
     * get the groups having the course
     *
     * @return a comma separated list of the groups having the groups formated in String
     */
    public String getGroups(){
        return groups;
    }

    /**
     * set the groups having the course
     * @param groups the groups having the course
     */
    public void setGroups(String groups){
        this.groups = groups;
    }

    /**
     * get the title of the course
     *
     * @return return the title of the course
     */
    public String getTitle(){
        return title;
    }


    /**
     * get the Type of the course
     *
     * @return return the type of the course
     */
    public String getType(){
        return type;
    }

    /**
     * get the teacher giving the course
     *
     * @return return the teacher giving the course
     */
    public String getTeacher(){
        return teacher;
    }

    /**
     * get the location of the course
     *
     * @return return the location of the course
     */
    public String getLocation(){
        return location;
    }

    /**
     * get the time the course starts
     *
     * @return return the time the course starts
     */
    public Time getStartTime(){
        return startTime;
    }

    /**
     * get the time the course ends
     *
     * @return return the time the course ends
     */
    public Time getEndTime(){
        return endTime;
    }

    /**
     * set the title of the course
     *
     * @param title the title of the course
     */
    public void setTitle(String title){
        this.title = title;
    }

    /**
     * set the type of the course
     *
     * @param type the type of the course
     */
    public void setType(String type){
        this.type = type;
    }

    /**
     * set the teacher of the course
     *
     * @param teacher the teacher of the course
     */
    public void setTeacher(String teacher){
        this.teacher = teacher;
    }

    /**
     * set the location of the course
     *
     * @param location the location of the course
     */
    public void setLocation(String location){
        this.location = location;
    }

    /**
     * set the Time the course starts
     *
     * @param startTime the time the course starts
     */
    public void setStartTime(Time startTime){
        this.startTime = startTime;
    }

    /**
     * set the Time the course ends
     *
     * @param endTime the time the course ends
     */
    public void setEndTime(Time endTime){
        this.endTime = endTime;
    }
}
