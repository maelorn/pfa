package com.eirbmmk.app.events;

import android.content.Context;
import android.text.format.Time;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eirbmmk.app.R;

import java.util.Calendar;
import java.util.Locale;


public class ExtendedCalendarView extends RelativeLayout implements AdapterView.OnItemClickListener, View.OnClickListener {
    private final Context context;
    private Calendar cal;
    private TextView month;
    private RelativeLayout base;
    private CalendarAdapter mAdapter;
    private GridView calendar;


    public ExtendedCalendarView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public ExtendedCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ExtendedCalendarView(Context context, AttributeSet attrs,int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    private void init(){
        cal = Calendar.getInstance();
        base = new RelativeLayout(context);
        base.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
        base.setMinimumHeight(50);
        base.setId(4);

        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.leftMargin = 16;
        params.topMargin = 50;
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        ImageView prev = new ImageView(context);
        prev.setId(1);
        prev.setLayoutParams(params);
        prev.setImageResource(R.drawable.navigation_previous_item);
        prev.setOnClickListener(this);
        base.addView(prev);

        params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.rightMargin = 16;
        params.topMargin = 50;
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        ImageView next = new ImageView(context);
        next.setId(3);
        next.setLayoutParams(params);
        next.setImageResource(R.drawable.navigation_next_item);
        next.setOnClickListener(this);
        base.addView(next);

        params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        month = new TextView(context);
        month.setId(2);
        month.setLayoutParams(params);
        // it doesn't work with this I don't know why
        //month.setTextAppearance(context, android.R.attr.textAppearanceLarge);

        month.setText(toTitle(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));
        month.setTextSize(30);

        base.addView(month);

        addView(base);


        params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 40, 0, 20);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.BELOW, base.getId());

        calendar = new GridView(context);
        calendar.setLayoutParams(params);
        calendar.setVerticalSpacing(4);
        calendar.setHorizontalSpacing(4);
        calendar.setNumColumns(7);
        calendar.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
        calendar.setDrawSelectorOnTop(true);
        calendar.setOnItemClickListener(this);
        mAdapter = new CalendarAdapter(context, cal);

        calendar.setAdapter(mAdapter);

        addView(calendar);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case 1:
                if(cal.get(Calendar.MONTH) == cal.getActualMinimum(Calendar.MONTH)) {
                    cal.set((cal.get(Calendar.YEAR)-1),cal.getActualMaximum(Calendar.MONTH),1);
                } else {
                    cal.set(Calendar.MONTH,cal.get(Calendar.MONTH)-1);
                }
                rebuildCalendar();
                break;
            case 3:
                if(cal.get(Calendar.MONTH) == cal.getActualMaximum(Calendar.MONTH)) {
                    cal.set((cal.get(Calendar.YEAR)+1),cal.getActualMinimum(Calendar.MONTH),1);
                } else {
                    cal.set(Calendar.MONTH,cal.get(Calendar.MONTH)+1);
                }
                rebuildCalendar();
                break;
            default:
                break;
        }
    }

    private void rebuildCalendar() {
        if(month != null){

            month.setText(toTitle(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())));
            refreshCalendar();
        }
    }

    private void refreshCalendar() {
        mAdapter.refreshDays();
        mAdapter.notifyDataSetChanged();
    }

    private String toTitle(String s){
        return s.substring(0,1).toUpperCase() + s.substring(1);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Time d = (Time) mAdapter.getItem(position);
        if(d != null){
            Toast.makeText(context, d.format("%d/%m/%y"), 3).show();
        }
    }
}
