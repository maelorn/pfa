package com.eirbmmk.app.map;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

/**
 * This class correspond to the custom view creating school's map
 */

public class MapView extends View {

    int svgHeight = 1400;
    int svgWidth = 1100;

    ArrayList<PolygonRoom> clickablePolygons = MapModel.getClickablePolygons();
    ArrayList<PolygonRoom> nonClickablePolygons = MapModel.getNonClickablePolygons();
    ArrayList<RectangleRoom> clickableRectangles = MapModel.getClickableRectangles();
    ArrayList<RectangleRoom> nonClickableRectangles = MapModel.getNonClickableRectangles();
    PolygonRoom contour = MapModel.getContour();
    boolean isLogged = MapController.isLogged();

    /* -------------------- Private classes -------------------- */

    /*
   * This class enables the custom view's canvas to be pinched to zoom
   * */
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            final float originalScale = mCalculatedScale;
            final float scale = detector.getScaleFactor();
            mScaleFactor = mScaleFactor*detector.getScaleFactor();
            mScaleFactor = Math.max(0.8f, Math.min(mScaleFactor, 1000.0f));
            //Center the zoom
                final float centerX = detector.getFocusX();
                final float centerY = detector.getFocusY();

                float diffX = centerX - mPosX;
                float diffY = centerY - mPosY;

                diffX = diffX * scale - diffX;
                diffY = diffY * scale - diffY;

                mPosX -= diffX;
                mPosY -= diffY;
                invalidate();

            //refresh canvas

            return true;
        }
    }

    /*
  * This class enables the custom view's canvas to be double tapped
  * */
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            final float x = e.getX();
            final float y = e.getY();
            mOffsetX = 0f;
            mOffsetY = 0f;
            mLastTouchX = x;
            mLastTouchY = y;
            mActivePointerId = e.getPointerId(0);



            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            //calculate position with translation and zoom
            float a = (e.getX()-mPosX)/(mScaleFactor* mCalculatedScale);
            float b = (e.getY()-mPosY)/(mScaleFactor* mCalculatedScale);

            //detection of the region and shows alertDialog
            for(int w = 0; w < clickableRectangles.size(); w++)
            {
                if(clickableRectangles.get(w).getRect().contains(a, b)){ //&& !moving){ //&& clickableRectangles.get(w).getState()!= 2) {
                    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle(clickableRectangles.get(w).getName());
                    alertDialog.show();
                }


            }
            for(int w = 0; w < clickablePolygons.size(); w++)
            {
                if(clickablePolygons.get(w).getRegion().contains((int) a, (int) b)){ //&& !moving) {
                    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle(clickablePolygons.get(w).getName());
                    alertDialog.show();

                }
            }
            return true;
        }

        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            float x = e.getX();
            float y = e.getY();
          if( mScaleFactor < 3.0){
            mScaleFactor = 3.0f;

            float diffX = x - mPosX;
            float diffY = y - mPosY;

            diffX = diffX * mScaleFactor - diffX;
            diffY = diffY * mScaleFactor - diffY;

            mPosX -= diffX;
            mPosY -= diffY;

            invalidate();
          }
            else if(mScaleFactor >= 3.0){
           mScaleFactor = 1f;
            mPosX = 0;
            mPosY = -100;

            invalidate();
          }
            Log.d("willou", "Tapped at: (" + x + "," + y + ")");

            return true;
        }
    }

    /* -------------------- Attributes -------------------- */

    /**
     * Personnalize design of the drawers
     */
    private Paint mYellow = new Paint();
    private Paint mBlackStroke = new Paint();
    private Paint mGrey = new Paint();
    private Paint mRed = new Paint();
    private Paint mGreen = new Paint();
    /**
     * TouchEvent management variables
     */
    //Detection of a movement
    private boolean moving = false;
    private float mOffsetX;
    private float mOffsetY;

    //Calculate movement coordinates
    private static final int INVALID_POINTER_ID = -1;
    private float mPosX;
    private float mPosY = -100.f;
    private float mLastTouchX;
    private float mLastTouchY;
    private int mActivePointerId = INVALID_POINTER_ID;

    //zoom management
    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;
    private int mCanvasWidth;
    private int mCanvasHeight;
    private float mCalculatedScale = 1.f; //to get the same scale factor on any different screen

    //double tap management
    private GestureDetector mGestureDetector;

    // User Status
    private boolean isConnected;

    Path mPath = new Path();
    Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public MapView(Context context, AttributeSet attrs, boolean connected) throws ParserConfigurationException, SAXException, IOException {
        super(context, attrs);
        //at first we show storey 0
        setStorey(0);
        isConnected = connected;
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        mGestureDetector = new GestureDetector(context, new GestureListener());


       /* String d = "M 623,519.4 C 623,1 520,3 640,11 z"; //ok
        String i = "m 670.3,518.1 h 0.6 v 10 h -0.6 v -10 z"; //ok
        String i1 = "m 631.4,519.4 c -0.3,0.4 -0.7,0.6 -1.2,0.9 z";
        String g = "M 631.4,519.4 L 640,500 L 690,520 z";
        String b = "m 623.4,528.2 c -0.7,0 -1.3,-0.2 -1.7,-0.5 -0.4,-0.3 -0.8,-0.7 -1.1,-1.2 -0.3,-0.5 -0.5,-1 -0.6,-1.6 -0.1,-0.6 -0.2,-1.2 -0.2,-1.8 0,-0.6 0.1,-1.2 0.2,-1.8 0.1,-0.6 0.3,-1.1 0.6,-1.6 0.3,-0.5 0.6,-0.9 1.1,-1.2 0.4,-0.3 1,-0.5 1.7,-0.5 0.7,0 1.3,0.2 1.7,0.5 0.4,0.3 0.8,0.7 1.1,1.2 0.3,0.5 0.5,1 0.6,1.6 0.1,0.6 0.2,1.2 0.2,1.8 0,0.6 -0.1,1.2 -0.2,1.8 -0.1,0.6 -0.3,1.1 -0.6,1.6 -0.3,0.5 -0.6,0.9 -1.1,1.2 -0.5,0.3 -1,0.5 -1.7,0.5 z";
        //m 0,-0.4 c 0.6,0 1.1,-0.1 1.5,-0.4 0.4,-0.3 0.7,-0.7 0.9,-1.1 0.2,-0.4 0.4,-0.9 0.5,-1.5 0.1,-0.5 0.1,-1 0.1,-1.5 0,-0.5 0,-1 -0.1,-1.5 -0.1,-0.5 -0.2,-1 -0.5,-1.5 -0.2,-0.4 -0.5,-0.8 -0.9,-1.1 -0.4,-0.3 -0.9,-0.4 -1.5,-0.4 -0.6,0 -1.1,0.1 -1.5,0.4 -0.4,0.3 -0.7,0.7 -0.9,1.1 -0.2,0.4 -0.4,0.9 -0.5,1.5 -0.1,0.5 -0.1,1 -0.1,1.5 0,0.5 0,1 0.1,1.5 0.1,0.5 0.2,1 0.5,1.5 0.2,0.4 0.5,0.8 0.9,1.1 0.4,0.2 0.9,0.4 1.5,0.4 z";
        String test = "m 631.4,519.4 c -0.3,0.4 -0.7,0.6 -1.2,0.9 -0.4,0.2 -0.9,0.3 -1.4,0.4 z";
        readPath(i1, mPath);

        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(0xff00aa00);
        mPaint.setStrokeWidth(1);*/



    }


    /* -------------------- Custom View Methods -------------------- */

    /**
     * Draws on the canvas
     * Modifies its appearance by zooming and translating
     * Defines paints parameters
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //saves the content to be able to zoom and translate
        canvas.save();
        contour = MapModel.getContour();

        mCalculatedScale = Math.min((float)canvas.getWidth()/(float)svgWidth, (float)canvas.getHeight()/(float)svgHeight);
        mCanvasWidth = canvas.getWidth();
        mCanvasHeight = canvas.getHeight();
        //translation
        canvas.translate(mPosX, mPosY);

        //zoom

        canvas.scale(mCalculatedScale *mScaleFactor, mCalculatedScale *mScaleFactor);


        //Paints parameters
        mYellow.setColor(Color.rgb(255, 222, 0));
        mRed.setColor(Color.rgb(255, 0, 0));
        mGreen.setColor(Color.rgb(0,200,83));
        mBlackStroke.setColor(Color.BLACK);
        mBlackStroke.setStyle(Paint.Style.STROKE);
        mBlackStroke.setStrokeJoin(Paint.Join.ROUND);
        mBlackStroke.setStrokeCap(Paint.Cap.ROUND);
        mBlackStroke.setStrokeWidth(3);
        mBlackStroke.setDither(true);
        mGrey.setColor(Color.rgb(200,200,200));



        //drawings
        if(contour != null)
            canvas.drawPath(contour.getPath(), mGrey);
       else
            Log.d("Map", "Contour problem");

        for(int w = 0; w < nonClickablePolygons.size(); w++)
        {
            canvas.drawPath(nonClickablePolygons.get(w).getPath(), mYellow);
            canvas.drawPath(nonClickablePolygons.get(w).getPath(), mBlackStroke);
        }

        for(int w = 0; w < clickablePolygons.size(); w++)
        {
            if(clickablePolygons.get(w).getState() == 1 && isLogged){
                canvas.drawPath(clickablePolygons.get(w).getPath(), mGreen);
                canvas.drawPath(clickablePolygons.get(w).getPath(), mBlackStroke);
            }
            else if(clickablePolygons.get(w).getState() == 0 && isLogged){
                canvas.drawPath(clickablePolygons.get(w).getPath(), mRed);
                canvas.drawPath(clickablePolygons.get(w).getPath(), mBlackStroke);
            }
            else{
            canvas.drawPath(clickablePolygons.get(w).getPath(), mYellow);
            canvas.drawPath(clickablePolygons.get(w).getPath(), mBlackStroke);
            }

        }

        for(int i = 0; i < nonClickableRectangles.size(); i++)
        {
            canvas.drawRect(nonClickableRectangles.get(i).getRect(), mYellow);
            canvas.drawRect(nonClickableRectangles.get(i).getRect(), mBlackStroke);

        }

        for(int i = 0; i < clickableRectangles.size(); i++)
        {
            if(clickableRectangles.get(i).getState() == 1 && isLogged){
                canvas.drawRect(clickableRectangles.get(i).getRect(), mGreen);
                canvas.drawRect(clickableRectangles.get(i).getRect(), mBlackStroke);
            }
            else if(clickableRectangles.get(i).getState() == 0 && isLogged){
                canvas.drawRect(clickableRectangles.get(i).getRect(), mRed);
                canvas.drawRect(clickableRectangles.get(i).getRect(), mBlackStroke);
            }
            else{
                canvas.drawRect(clickableRectangles.get(i).getRect(), mYellow);
                canvas.drawRect(clickableRectangles.get(i).getRect(), mBlackStroke);
            }


        }

       // canvas.drawPath(mPath, mPaint);

        canvas.restore();

    }




    /**
     * Manages events on the canvas
     */
    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        //zoom detector
        mScaleDetector.onTouchEvent(ev);
        mGestureDetector.onTouchEvent(ev);

        //events management on the canvas
        final int action = ev.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {

                break;
            }

            case MotionEvent.ACTION_MOVE: {
                final int pointerIndex = ev.findPointerIndex(mActivePointerId);
                final float x = ev.getX(pointerIndex);
                final float y = ev.getY(pointerIndex);

                // Only move if the ScaleGestureDetector isn't processing a gesture.
                if (!mScaleDetector.isInProgress()) {

                    //realtime translation
                    final float dx = x - mLastTouchX;
                    final float dy = y - mLastTouchY;

                    //store the final state of the translation
                    mPosX += dx;
                    mPosY += dy;
                    mOffsetX += Math.abs(dx);
                    mOffsetY += Math.abs(dy);

                    //detection of a movement or just an action down
                    if(mOffsetX > 25 || mOffsetY > 25){
                        moving = true;
                    }
                    else{
                        moving = false;
                    }

                    //Limit the translation of the map vertically and horizontally
                    float horizontalLimit = (mScaleFactor/2)*svgWidth;
                    float verticalLimit = (mScaleFactor/2)*svgHeight;
                    if(mPosX > horizontalLimit)
                        mPosX = horizontalLimit;
                    if(mPosX < -horizontalLimit)
                        mPosX = -horizontalLimit;
                    if(mPosY > verticalLimit)
                        mPosY = verticalLimit;
                    if(mPosY < -verticalLimit -100)
                        mPosY = -verticalLimit -100;

                    invalidate();
                }

                mLastTouchX = x;
                mLastTouchY = y;

                break;
            }

            case MotionEvent.ACTION_UP: {
                mActivePointerId = INVALID_POINTER_ID;


                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }

            //a finger leaves the screen but at least one finger is still touching it.
            case MotionEvent.ACTION_POINTER_UP: {
                final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK)
                        >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = ev.getPointerId(pointerIndex);
                if (pointerId == mActivePointerId) {
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchX = ev.getX(newPointerIndex);
                    mLastTouchY = ev.getY(newPointerIndex);
                    mActivePointerId = ev.getPointerId(newPointerIndex);
                }
                break;
            }
        }

        return true;
    }



    /* -------------------- Methods --------------------
    /**
     * public method to select the storey to show according to the selected button above the custom view
     * @param storey selected from button
     */
    public void setStorey(int storeyNb){
        try {
            MapModel.svgParsingStorey(storeyNb);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        invalidate();
    }

    /**
     * public method to restore original scale and screen
     */
    public void setOriginalScale(){
        mScaleFactor = 1f;
        mPosX = 0;
        mPosY = -100;
        invalidate();
    }

    private void readPath(String data, Path p) {
        try {
            float x = 0;
            float y = 0;
            float x1 = 0;
            float y1 = 0;
            float x2 = 0;
            float y2 = 0;
            float x3 = 0;
            float y3 = 0;
            String[] tokens = data.split("[ ,]");
            int i = 0;
            Log.d("willou", "Nouvelle série");

            while (i < tokens.length) {
                Log.d("willou", ""+ tokens[i]);
                String token = tokens[i++];
                if (token.equals("M") || token.equals("m")) {
                    x = Float.valueOf(tokens[i++]);
                    y = Float.valueOf(tokens[i++]);
                    p.moveTo(x, y);
                } else
                if (token.equals("L") || token.equals("l")) {
                    x = Float.valueOf(tokens[i++]);
                    y = Float.valueOf(tokens[i++]);
                    p.lineTo(x, y);
                } else
                if (token.equals("C") || token.equals("c")) {

                    do{

                            x1 = Float.valueOf(tokens[i++]);
                        Log.d("willou", " " + x1);
                            y1 = Float.valueOf(tokens[i++]);
                        Log.d("willou", " " + y1);
                            x2 = Float.valueOf(tokens[i++]);
                        Log.d("willou", " " + x2);
                            y2 = Float.valueOf(tokens[i++]);
                        Log.d("willou", " " + y2);
                            x3 = Float.valueOf(tokens[i++]);
                        Log.d("willou", " " + x3);
                        y3 = Float.valueOf(tokens[i++]);
                        Log.d("willou", " " + x3);
                            p.cubicTo(x1, y1, x2, y2, x3, y3);
                    } while(!tokens[i].equals("M") || !tokens[i].equals("m") || !tokens[i].equals("L") || !tokens[i].equals("l") || !tokens[i].equals("C") || !tokens[i].equals("c") || !tokens[i].equals("H") || !tokens[i].equals("H") || !tokens[i].equals("V") || !tokens[i].equals("v") || !tokens[i].equals("z") || !tokens[i].equals("Z"));
                }else
                if (token.equals("H") || token.equals("h")) {
                    float a = Float.valueOf(tokens[i++]);
                    x= x + a;
                    p.lineTo(x, y);
                }
                else
                if (token.equals("V") || token.equals("v")) {
                    float b = Float.valueOf(tokens[i++]);
                    y = y + b;
                    p.lineTo(x, y);
                }else
                if (token.equals("z") || token.equals("Z")) {
                    p.close();
                } else {
                    throw new RuntimeException("unknown command [" + token + "]");
                }
            }
        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("bad data ", e);
        }
    }


}

