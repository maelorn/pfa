package com.eirbmmk.app.connection;

/**
 * Contains all the needed information for user authentication on the CAS or Eirb'Auth using
 * the O'Auth authentication protocol.
 */
public class ConnectionModel {


    /* -------------------- Attributes -------------------- */

    /**
     * Application ID for OAuth eirb.fr
     */
    private String mClientID;

    /**
     * Application secret for oAuth eirb.fr
     */
    private String mClientSecret;

    /**
     * Callback URL to use for OAuth authentication
     */
    private String mCallbackUrl;

    /**
     * Authorization URL
     */
    private String mAuthUrl;

    /**
     * Token recovery URL
     */
    private String mTokenBaseUrl;


    /* -------------------- Constructors -------------------- */

    public ConnectionModel()
    {
        mClientID = "d7226efeac0b8b1cde84bf6199942d1c3fcbff3b";
        mClientSecret = "4d7be2934322a157d716d30ec3c8a9ea3060fbafa1aaee9909dfd0d53cefe8cecca1f228e0686ced6c56023f2c3b77eaa32583951c824494ffc95fd45f198817";
        mCallbackUrl = "x-fr.eirb.auth.mobileapp://oauth/callback";
        //mAuthUrl = "https://auth.eirb.fr/connexion?next=/oauth/authorize/%3Fresponse_type%3Dcode%26client_id%3Dd7226efeac0b8b1cde84bf6199942d1c3fcbff3b%26redirect_uri%3Dx-fr.eirb.auth.mobileapp%253A%252F%252Foauth%252Fcallback%26state%3D";
        mAuthUrl = "https://auth.eirb.fr/oauth/authorize/?response_type=code&client_id=d7226efeac0b8b1cde84bf6199942d1c3fcbff3b&redirect_uri=x-fr.eirb.auth.mobileapp%3A%2F%2Foauth%2Fcallback";
        mTokenBaseUrl = "https://auth.eirb.fr/oauth/token/";

    }

    /* -------------------- Methods -------------------- */

    /**
     * Return the authorization URL to use for user identification on CAS/ Eirb'Auth
     *
     * @return the authorization URL
     */
    public String getAuthorizationUrl()
    {
        return mAuthUrl;
    }

    /**
     * Return the token base URL to get the access token to Eirb.fr web-services
     *
     * @return the token base URL
     */
    public String getTokenBaseUrl()
    {
        return mTokenBaseUrl;
    }

    /**
     * Return application client ID to use for eirb.fr authentication.
     *
     * @return the Client ID
     */
    public String getClientID()
    {
        return mClientID;
    }

    /**
     * Return application client secret to use for eirb.fr authentication.
     *
     * @return the Client Secret
     */
    public String getClientSecret()
    {
        return mClientSecret;
    }

    /**
     * Return callback URL to use for eirb.fr authentication.
     *
     * @return the callback URL
     */
    public String getCallbackUrl()
    {
        return mCallbackUrl;
    }
}
