package com.eirbmmk.app.connection;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;

public class ConnectionFragment extends BaseFragment {

    /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Connection pattern
     */
    private ConnectionController mController;


        /* -------------------- Constructors -------------------- */

    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public ConnectionFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }

    /**
     * Constructor of the ConnectionFragment class
     *
     * @param container the controller associated to the MainActivity of the application
     * @param model the model including connection information
     */
	public ConnectionFragment(MainActivityController container, ConnectionModel model)
    {
        mController = new ConnectionController(container, model, this);
        super.setBaseController(mController);
    }

        /* -------------------- Methods -------------------- */


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;


        if (!mController.isNetworkConnected()){
            mRootView = inflater.inflate(R.layout.no_internet_connection, container, false);
            return mRootView;
        }

        mRootView = inflater.inflate(R.layout.fragment_connection, container, false);

        WebView authWebView = (WebView) mRootView.findViewById(R.id.authWebView);
        authWebView.getSettings().setAppCacheEnabled(false);
        authWebView.getSettings().setSaveFormData(false);

        String authUrl = mController.getAuthorizationUrl();

        mSpinner = (ProgressBar) mRootView.findViewById(R.id.connection_loading_spinner);

        //block the launch into the full browser
        authWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                super.onPageStarted(view, url, favicon);
                mSpinner.setVisibility(View.VISIBLE);
                //android.util.Log.i("TEST", "Page started : " + url);

            }

            @Override
            public void onPageFinished(WebView view, String url) {

                mSpinner.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading (WebView view, String url)
            {
//                try {
//                    //android.util.Log.i("TEST", "Page Loading : " + URLDecoder.decode(url, "UTF-8"));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                Uri response = Uri.parse(url);

                if (response.getScheme().equals("x-fr.eirb.auth.mobileapp")) { // authorization succeeded
//                    new android.app.AlertDialog.Builder(getActivity())
//                            .setMessage(url)
//                            .setTitle("Connexion")
//                            .setCancelable(false)
//                            .setNeutralButton(android.R.string.ok,
//                                    new android.content.DialogInterface.OnClickListener() {
//                                        public void onClick(android.content.DialogInterface dialog, int whichButton){}
//                                    })
//                            .show();

                    final String code = response.getQueryParameter("code");

                    if (code == null)
                    {
                        displayErrorMessage("Echec de l'authentification.");
                        android.util.Log.i("ERROR CONNECTION", "code pour recuperation du token inexistant :");
                        return true;
                    }

                    mSpinner.setVisibility(View.VISIBLE);
                    view.setVisibility(View.GONE);
                    view.destroy();

                    mController.finishAuthentication(code);

                    return true;
                }

                // other page, load it
                view.loadUrl(url);
                return true;
            }

        });
       // android.util.Log.i("OOOOOOAAAAA", authUrl);
        // load the page
        authWebView.loadUrl(authUrl);
        return mRootView;
    }

    /**
     * Delete all cookies created
     */
    public void deleteCookies()
    {
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
    }

    /**
     * Displays a message to indicates to the user that the connection succeeds.
     *
     */
    public void displayConnectionSuccess()
    {
        mRootView.findViewById(R.id.connection_text_success).setVisibility(View.VISIBLE);
    }
}
