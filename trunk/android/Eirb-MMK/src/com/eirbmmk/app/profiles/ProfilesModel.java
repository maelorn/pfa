package com.eirbmmk.app.profiles;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Contains all the needed information for Profiles functionality like profiles webservices URL.
 */
public class ProfilesModel {

    /* -------------------- Attributes -------------------- */

    private static final String mClubsURL = "http://appli.eirbmmk.fr/Clubs/";
    private static final String mProfilesURL = "http://appli.eirbmmk.fr/Profiles/";

    /* -------------------- Constructors -------------------- */

    public ProfilesModel()
    {
    }
    /* -------------------- Methods -------------------- */

    /**
	* getting clubs from webservice
	*
	*@return the list of clubs
	*/
    public ArrayList<Clubs> getClubs()
    {
        ArrayList<Clubs> clubs = new ArrayList<Clubs>();

        HttpClient httpclient = new DefaultHttpClient();
        String jsonResult = null;

        // HTTP POST Creation request
        HttpPost httpPost = new HttpPost(mClubsURL);
        HttpResponse httpResponse = null;

        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        // Sends HTTP POST Request
        try {
            httpResponse = httpclient.execute(httpPost);
            // get HTTP POST result
            jsonResult = EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e) {
            android.util.Log.e("ERROR", e.toString());
            e.printStackTrace();
            return null;
        }

        try {
            JSONArray dataArray = new JSONArray(jsonResult);

            for(int i = 0 ; i < dataArray.length() ; i++){
                JSONObject object = (JSONObject) dataArray.get(i);
                Clubs clubToAdd = new Clubs(object.getString("id"), object.getString("name"), object.getString("icon"));
                Log.d("Insert", "" + object.getString("icon"));
                clubs.add(clubToAdd);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return clubs;
    }

    /**
     * getting user clubs from profiles webservice
     * @param clubsName name of user's clubs
     * @return the list of clubs
     */
    public ArrayList<Clubs> getMyClubs(ArrayList<String> clubsName) {
        ArrayList<Clubs> clubs = new ArrayList<Clubs>();

        HttpClient httpclient = new DefaultHttpClient();
        String jsonResult = null;

        // HTTP POST Creation request
        HttpPost httpPost = new HttpPost(mClubsURL);
        HttpResponse httpResponse = null;

        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        // Sends HTTP POST Request
        try {
            httpResponse = httpclient.execute(httpPost);
            // get HTTP POST result
            jsonResult = EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e) {
            android.util.Log.e("ERROR", e.toString());
            e.printStackTrace();
            return null;
        }

        try {
              JSONArray dataArray = new JSONArray(jsonResult);

            for(int i = 0 ; i < dataArray.length() ; i++){
                JSONObject object = (JSONObject) dataArray.get(i);
                for (int j = 0 ; j<clubsName.size() ; j++) {
                    if (object.getString("id").equals(clubsName.get(j))) {
                        //Log.w(getClass().getName(), "NAME : "+ object.getString("name") + " " + clubsName.get(j));
                        Clubs clubToAdd = new Clubs(object.getString("id"), object.getString("name"), object.getString("icon"));
                        //Log.w(getClass().getName(), "NAME : " + object.getString("id") + object.getString("name") + object.getString("icon"));
                        Log.d("Insert", "" + object.getString("icon"));
                        clubs.add(clubToAdd);
                        j = clubsName.size() + 1;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            //return null;
        }

        return clubs;
    }

    /**
     * getting user clubs' names from profiles webservice
     * @param login user's login
     * @return the list of clubs' names
     */
    public ArrayList<String> getMyClubsName(String login) {

        ArrayList<String> clubsName = new ArrayList<String>();
        HttpClient httpclient = new DefaultHttpClient();
        String jsonResult = null;

        // HTTP POST Creation request
        HttpPost httpPost = new HttpPost(mProfilesURL);
        HttpResponse httpResponse = null;

        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        // Sends HTTP POST Request
        try {
            httpResponse = httpclient.execute(httpPost);
            // get HTTP POST result
            jsonResult = EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e) {
            android.util.Log.e("ERROR", e.toString());
            e.printStackTrace();
            return null;
        }

        try {
            JSONArray dataArray = new JSONArray(jsonResult);
            for(int i = 0 ; i < dataArray.length() ; i++){
                JSONObject object = (JSONObject) dataArray.get(i);

                //JSONObject object = new JSONObject(jsonResult);
                if (object.getString("login").equals(login)) {
                    JSONArray clubs = object.getJSONArray("clubs");
                    for (int j=0; j<clubs.length() ; j++) {
                        //clubsName.add(clubs.getString(j).toString());
                        clubsName.add(clubs.getString(j));
                    }
                    i = dataArray.length() +1;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        Log.w(getClass().getName(), "CLUBS NAME :" + clubsName);
        return clubsName;
    }


    /**
     * getting information of profile from webserivce
     * @param login user's login
     * @return the list of information that we need (name, promo, biography etc)
     */
    public ArrayList<String> getProfileInformation(String login) {

        ArrayList<String> information = new ArrayList<String>();
        HttpClient httpclient = new DefaultHttpClient();
        String jsonResult = null;

        // HTTP POST Creation request
        HttpPost httpPost = new HttpPost(mProfilesURL);
        HttpResponse httpResponse = null;

        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        // Sends HTTP POST Request
        try {
            httpResponse = httpclient.execute(httpPost);
            // get HTTP POST result
            jsonResult = EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e) {
            android.util.Log.e("ERROR", e.toString());
            e.printStackTrace();
            return null;
        }

        try {
            JSONArray dataArray = new JSONArray(jsonResult);
            for(int i = 0 ; i < dataArray.length() ; i++){
                JSONObject object = (JSONObject) dataArray.get(i);

                //JSONObject object = new JSONObject(jsonResult);
                if (object.getString("login").equals(login)) {

                    information.add(object.getString("login"));
                    information.add(object.getString("firstname"));
                    information.add(object.getString("name"));
                    information.add(object.getString("filiere"));
                    information.add(object.getString("promo"));
                    information.add(object.getString("bio"));
                    information.add(object.getString("website"));
                    information.add(object.getString("pic"));
                    i = dataArray.length() +1;
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return information;
    }

}

