package com.eirbmmk.app.profiles;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.eirbmmk.app.BaseSecondaryFragment;
import com.eirbmmk.app.R;

import java.util.ArrayList;

/**
 * Club
 */
public class ClubFragment extends BaseSecondaryFragment {

    /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Clubs pattern
     */
    private ProfilesController mController;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public ClubFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }

    public ClubFragment(ProfilesController controller) {
        mController = controller;
        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait its recreation
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        mRootView = inflater.inflate(R.layout.fragment_clubs, container, false);

        if(!mController.isNetworkConnected()){
            mRootView = inflater.inflate(R.layout.no_internet_connection, container, false);
        } else {
            mController.getClubsList();
        }

        return mRootView;
    }

    /**
     * set clubs' list in ClubFragment view
     *
     * @param clubs the clubs' list
     */
    public void displayClubsList(final ArrayList<Clubs> clubs)
    {
        ClubListAdapter adapter = new ClubListAdapter(mRootView.getContext(),clubs);
        ListView listView = (ListView) mRootView.findViewById(R.id.clubsListView);
        listView.setAdapter(adapter);
        listView.invalidate();

        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // il faut gérer l'ajout de l'asso à la liste
                //mController.addClubToMyList(clubs.get(position));
                mController.displayProfilesFragment();
            }
        });
    }
}
