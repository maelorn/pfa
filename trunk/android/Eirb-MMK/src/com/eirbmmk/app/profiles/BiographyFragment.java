package com.eirbmmk.app.profiles;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.eirbmmk.app.BaseSecondaryFragment;
import com.eirbmmk.app.R;

import java.util.ArrayList;

/**
 * Biography fragment to edit user biography
 */
public class BiographyFragment extends BaseSecondaryFragment {

    /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Clubs pattern
     */
    private ProfilesController mController;

    /**
     * The biography
     */
    private String mTextBiography;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public BiographyFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }

    public BiographyFragment(ProfilesController controller, String text) {
        mController = controller;
        mTextBiography = text;
        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        mRootView = inflater.inflate(R.layout.biography, container, false);

        //mController.getTextForBibliography();

        Button OkBiography = (Button) mRootView.findViewById(R.id.okBiography);
        EditText EditTextBibliography = (EditText) mRootView.findViewById((R.id.editBiography));
        EditTextBibliography.setText(mTextBiography);
        OkBiography.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // modify biography
                // THEN
                mController.displayProfilesFragment();
                //mController.validateTextForBibliography;
            }
        });

        return mRootView;
    }

}

