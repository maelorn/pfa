package com.eirbmmk.app.profiles;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ProfilesFragment extends BaseFragment {

	 /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Connection pattern
     */
    private ProfilesController mController;

    /**
    * some static attributes
    */
    private static final int REQUEST_ID = 1;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public ProfilesFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }

    /**
     * Constructor of the ProfilesFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including profiles information.
     */
    public ProfilesFragment(MainActivityController container, ProfilesModel model)
    {
        mController = new ProfilesController(container, model, this);
        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        /* ------ Functionality still in development ------- */

        //mRootView = inflater.inflate(R.layout.fragment_in_development, container, false);
        //return mRootView;

        /* ------ Uncomment to continue development ------- */

        mRootView = inflater.inflate(R.layout.fragment_profiles, container, false);

        if(!mController.isNetworkConnected()){
            mRootView = inflater.inflate(R.layout.no_internet_connection, container, false);
        }
        else {
            mController.setProfile();
        }

        return mRootView;
    }


    /**
     * action when clicking on the imageView
     */
    public void browsePhoto() {

        ImageView photoView = (ImageView) mRootView.findViewById(R.id.photoProfile);

        /* try to browse a Photo from mobile when clicking on the Image */
        photoView.setOnClickListener(new ImageView.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_ID);
                        /* the code of the resulting process is in "onActivityResult" method */
                        /* Ok that's working */
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        InputStream stream = null;
        if (requestCode == REQUEST_ID && resultCode == Activity.RESULT_OK) {
            try {
                stream = getActivity().getContentResolver().openInputStream(data.getData());
                Bitmap original = BitmapFactory.decodeStream(stream);
                // Must MANAGE more efficiently the dimensions of the resulting photo
                ((ImageView) getView().findViewById(R.id.photoProfile)).setImageBitmap(Bitmap.createScaledBitmap(original, original.getWidth(), original.getHeight(), true));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (stream != null) {
                try {
                    stream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            /* problem: the context is not saved */
        }
    }


    /**
     * action when clicking on mailto button
     * @param login the user's login
     */
    public void sendMail(final String login) {

        ImageView mailTo = (ImageView) mRootView.findViewById(R.id.mailto);
        mailTo.setOnClickListener(new ImageView.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                intent.setType("plain/text");
                //intent.putExtra(Intent.EXTRA_EMAIL, new String[]{login + "@enseirb-matmeca.fr"});
                //intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                //intent.putExtra(Intent.EXTRA_TEXT, "mail body");
                startActivity(Intent.createChooser(intent, ""));
                /* Ok that's working */
            }
        });

    }

    /**
     * action when clicking on website button
     */
    public void goToWebSite(final String website) {

        final ImageView webSite = (ImageView) mRootView.findViewById(R.id.ic_website);
        webSite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.w("WEBSITE :", website);
                try {
                    if (webSite.equals(""))
                        Toast.makeText(getActivity().getApplicationContext(), "Vous n'avez pas de site web",
                                Toast.LENGTH_LONG).show();
                    else {
                    Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setData(Uri.parse(website)); // test value for website
                        startActivity(intent);
                         /* Ok that's working */
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * action when clicking on add biography button
     * @param text biography text to set
     */
    public void modifyBiography(final String text) {

        Button buttonBibliography = (Button) mRootView.findViewById(R.id.ButtonBiography);
        buttonBibliography.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                mController.displayBibliographyFragment(text);
            }
        });
    }

    /**
     * action when clicking on add association button
     */
    public void addClubs() {
        Button addAssociation = (Button) mRootView.findViewById(R.id.addAssociation);
        addAssociation.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                mController.displayClubFragment();
            }
        });
    }

    /**
     * action when clicking on delete association button
     */
    public void removeClubs() {
        Button delAssociation = (Button) mRootView.findViewById(R.id.deleteAssociation);
        delAssociation.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if (!(mController.displayDeleteClubFragment()))
                    Toast.makeText(getActivity().getApplicationContext(), "Vous n'appartenez à aucune association",
                            Toast.LENGTH_LONG).show();

            }
        });
    }

    /**
     * display schedule fragment from profile fragment
     */
    public void displaySchedule() {
        ImageView schedule = (ImageView) mRootView.findViewById((R.id.edt));
        schedule.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                mController.displayScheduleFragment();
            }
        });
    }

    /**
     * display profile information such as user identity, text of biography ond clubs
     * @param clubs list of clubs to add to profile's view
     * @param information list of information about user (name, promo, biography etc..)
     */
    public void displayProfileInformation(final ArrayList<String> information, ArrayList<Clubs> clubs){

        TextView login = (TextView) mRootView.findViewById(R.id.login);
        login.setText("~" + information.get(0));

        TextView name = (TextView) mRootView.findViewById(R.id.lastAndFirstName);
        name.setText(information.get(1) + " " + information.get(2));

        TextView promo = (TextView) mRootView.findViewById(R.id.spinneret);
        promo.setText(information.get(3) + " - " + information.get(4));

        ImageView photo = (ImageView) mRootView.findViewById(R.id.photoProfile);

        new DownloadImageTask(photo).execute(information.get(7));

        final TextView textBibliography = (TextView) mRootView.findViewById(R.id.textBiography);
        if (information.get(5) != null)
            textBibliography.setText(information.get(5));
        else
            textBibliography.setHint("Ajoutez votre biographie ici.");
        textBibliography.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                mController.displayBibliographyFragment(textBibliography.getText().toString());
            }
        });
        mController.modifyBiography(textBibliography.getText().toString());

        ClubListAdapter adapter = new ClubListAdapter(mRootView.getContext(), clubs);
        ListView listView = (ListView) mRootView.findViewById(R.id.listAssociation);
        listView.setAdapter(adapter);
        listView.invalidate();

    }

    /**
     * Private Class to simplify Image Downloading from the Internet
     */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urlDisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urlDisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


}
