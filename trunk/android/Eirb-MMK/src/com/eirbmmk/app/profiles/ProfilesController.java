package com.eirbmmk.app.profiles;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.schedule.ScheduleFragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.Screen;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * This class correspond to the controller part of the MVC design pattern for Profiles
 * functionality.
 */
public class ProfilesController extends BaseController {

    /* -------------------- Attributes -------------------- */


    /**
    * Model part of the MVC design pattern
    */
    private ProfilesModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private ProfilesFragment mFragment;


    /**
     * Reference to a possible secondary  fragment to display club
     */
    private ClubFragment mClubFragment;

    /**
     * Reference to a possible secondary fragment to display biography
     */
    private BiographyFragment mBiographyFragment;

    /**
     * Reference to a possible secondary fragment to delete clubs
     */
    private DeleteClubFragment mDeleteClubFragment;

    /**
     * Reference to a possible secondary fragment to display user schedule
     */
    private ScheduleFragment mScheduleFragment;

      /* -------------------- Constructors -------------------- */


    /**
     * Constructor of ProfilesController class.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the Profiles functionality
     * @param fragment the view of the Profiles functionality
     */
    public ProfilesController(MainActivityController container, ProfilesModel model, ProfilesFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;
    }


    /* -------------------- Methods -------------------- */

    /**
    * set Profile information such as the full name, login, biography etc
    * */
    public void setProfile() {
        AsyncTask<Void, Void, Boolean> profilesRequest = new AsyncTask<Void, Void, Boolean>() {

            // mInformation is an array of Strings which will
            // contain all the information taken from the webservice
            private ArrayList<String> mInformation = null;
            private ArrayList<Clubs> mClubs = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mFragment.showLoadingSpinner();
            }

            @Override
            protected Boolean doInBackground(Void... Params) {
                if (!isCancelled()) {

                    Log.w(ProfilesModel.class.toString(), "USERNAME : " + mMainActivityController.getUserLogin());
                    // login = mMainActivityController.getUserLogin() // =========== YES));
                    // getProfileInformation return an array of strings containing :
                    // first name, last name, login, promo and biography
                    /*
                    ArrayList<String> mTest = new ArrayList<String>();
                    mTest.add("BDA");
                    mTest.add("BDE");
                    mTest.add("AEI");
                    */
                    mInformation = mModel.getProfileInformation("mphalippou");
                    //mClubs = mModel.getMyClubs(mTest);
                    // ATTENTION : have to change the list test of clubs then use this call
                    mClubs = mModel.getMyClubs(mModel.getMyClubsName("mphalippou"));
                }
                return (mInformation != null);
            }

            @Override
            protected void onPostExecute(Boolean ok) {
                if (ok.booleanValue()) {
                    // setting all information that we need from the webservice
                    mFragment.displayProfileInformation(mInformation, mClubs);
                    // allowing to browse a photo
                    mFragment.browsePhoto();
                    // allowing to send an email
                    mFragment.sendMail("mphalippou"); // =================== TO CHANGE WITH mMainActivityController.getUserLogin()
                    // allowing the access to the user website
                    mFragment.goToWebSite(mInformation.get(6));

                    // allowing the user to add clubs to his list
                    mFragment.addClubs();
                    // allowing the user to remove clubs from his list
                    mFragment.removeClubs();

                    mFragment.displaySchedule();
                } else {
                    // display an error message
                    mFragment.displayErrorMessage("Un problème est survenu lors de la récupération des informations concernant votre profil");
                }

                mFragment.hideLoadingSpinner();

                // remove the task to running tasks list
                mAsyncRunningTasks.remove(this);
            }
        };

        // add the task to running tasks list
        mAsyncRunningTasks.add(profilesRequest);

        // start the async task
        profilesRequest.execute();
    }

    /**
	* this method is called to set the list of clubs from webservice
	*/
    public void getClubsList()
    {
        AsyncTask<Void, Void, Boolean> clubsRequest = new AsyncTask<Void, Void, Boolean>() {

            // mClubs will contain all clubs from the webservice
            private ArrayList<Clubs> mClubs = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mClubFragment.showLoadingSpinner();
            }

            @Override
            protected Boolean doInBackground(Void... Params) {
                if (!isCancelled()) {
                    // getting clubs from the webservice
                    mClubs = mModel.getClubs();
                }
                return mClubs != null;
            }

            @Override
            protected void onPostExecute(Boolean ok) {
                if (ok.booleanValue()) {
                    // setting clubs list
                    mClubFragment.displayClubsList(mClubs);
                } else {
                    mClubFragment.displayErrorMessage("Un problème est survenu lors de la récupération de la liste des Clubs & Associations.");
                }

                mClubFragment.hideLoadingSpinner();

                // remove the task to running tasks list
                mAsyncRunningTasks.remove(this);
            }
        };

        // add the task to running tasks list
        mAsyncRunningTasks.add(clubsRequest);

        // start the async task
        clubsRequest.execute();
    }

    /**
     * Function called for setting the clubs to which the user belong in order to choose one of them to remove
     */
    public void setMyClubsListToRemove() {
        AsyncTask<Void, Void, Boolean> clubsRequest = new AsyncTask<Void, Void, Boolean>() {

            // mClubs will contain all clubs from the webservice
            private ArrayList<Clubs> mClubs = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mDeleteClubFragment.showLoadingSpinner();
            }

            @Override
            protected Boolean doInBackground(Void... Params) {

                if (!isCancelled()) {
                    // getting clubs from the webservice

                    /*
                    ArrayList<String> mTest = new ArrayList<String>();
                    mTest.add("BDA");
                    mTest.add("BDE");
                    mTest.add("AEI");
                    */
                    //mClubs = mModel.getMyClubs(mTest);
                    mClubs = mModel.getMyClubs(mModel.getMyClubsName("mphalippou"));
                }
                return mClubs != null;
            }

            @Override
            protected void onPostExecute(Boolean ok) {
                if (ok.booleanValue()) {
                    // setting clubs list
                    mDeleteClubFragment.displayMyClubsList(mClubs);
                } else {
                    mDeleteClubFragment.displayErrorMessage("Un problème est survenu lors de la récupération de la liste des Clubs & Associations.");
                }

                mDeleteClubFragment.hideLoadingSpinner();

                // remove the task to running tasks list
                mAsyncRunningTasks.remove(this);
            }
        };

        // add the task to running tasks list
        mAsyncRunningTasks.add(clubsRequest);

        // start the async task
        clubsRequest.execute();

    }

    /**
     * switching to biographyFragment
     * @param text the biography to display
     */
    public void displayBibliographyFragment(String text)
    {
        mBiographyFragment = new BiographyFragment(this, text);
        mMainActivityController.displaySecondaryFragment(mBiographyFragment);
    }

    /**
     * switching to clubFragment
     */
    public void displayClubFragment()
    {
        mClubFragment = new ClubFragment(this);
        mMainActivityController.displaySecondaryFragment(mClubFragment);
    }

    /**
     * recreating profile fragment from another view
     */
    public void displayProfilesFragment() {
        mMainActivityController.recreateCurrentMVCFragment();
    }

    /**
     * switching to deleteClubFragment
     */
    public boolean displayDeleteClubFragment()
    {
        //if (mModel.getMyClubsName("mphalippou") != null) { // TO CHANGE
            mDeleteClubFragment = new DeleteClubFragment(this);
            mMainActivityController.displaySecondaryFragment(mDeleteClubFragment);
            return true;
        //}
        //return false;
    }

    /**
     * displaying scheduleFragment from profilesFragment
     */
    public void displayScheduleFragment()
    {
        mMainActivityController.createMVCFragment(Screen.SCHEDULE);
    }

    /**
     * action on biography button click
     * @param text text of biography to set
     */
    public void modifyBiography(String text) {
        mFragment.modifyBiography(text);
    }

}
